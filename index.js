var po2json = require('po2json');
var loaderUtils = require('loader-utils');

module.exports = function(source) {
  var options = loaderUtils.getOptions(this);
  var catalog = po2json.parse(source, {
    format: 'jed1.x',
    domain: options.domain || 'messages',
  });

  this.cacheable();

  return 'module.exports = ' + JSON.stringify(catalog) + ';';
}
